FROM centos:7
MAINTAINER Antoni Segura Puimedon <antonisp@celebdor.com>

ADD conf/delorean.repo /etc/yum.repos.d/
RUN yum update -y \
  \
  && yum group install -y \
  "Development Tools" \
  \
  && yum install -y \
  epel-release \
  python2-rpm-macros \
  python3-rpm-macros \
  python3-pkgversion-macros \
  python2-devel \
  python34-devel \
  rpmdevtools \
  \
  && useradd -d /home/builder -u 1000 -U builder

VOLUME /home/builder

USER builder:builder
ENTRYPOINT ["/usr/bin/rpmbuild"]
