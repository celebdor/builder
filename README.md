RPM Builder
===========

This container is for being able to use rpmbuild in any distro.

Usage
-----

Copy rpmbuild anywhere on your path (like ~/bin) and then just run rpmbuild
commands normally like:

    rpmbuild --help
    rpmbuild -ba foo.spec
